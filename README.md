Demonstrates a geogebra item with unintentional cropping with use of a scrollbar.

[Live demo](https://clever-hoover-6de9be.netlify.com/)

[Geogebra help forum topic](https://help.geogebra.org/topic/scaling-embedded-app-with-toolbar)
