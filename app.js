// Creates Geogebra applet via window.GGBApplet and
// injects the applet to the given instanceId

const log = console.log;

const createApplet = (params) => {
  const {
    materialId,
    instanceId,
    showToolBar,
    width,
    height,
    scaleContainerClass,
    disableAutoScale,
  } = params;

  const options = {
    material_id: (materialId || '').split('/').pop(),
    id: instanceId,
    showToolBar,
    disableAutoScale,
    scaleContainerClass,
    allowUpscale: true,
    showMenuBar: false,
    showResetIcon: true,
    enable3d: true,
    enableFileFeatures: false,
    enableShiftDragZoom: false,
    enableRightClick: false,
    showToolBarHelp: false,
    allowStyleBar: false,
    algebraInputPosition: 'top',
    borderColor: 'FFFFFF',
  };

  if (disableAutoScale) {
    Object.assign(options, {
      width,
      height,
      scaleContainerClass: '',
    });
  }

  return new Promise((resolve) => {
    log('requesting', materialId, instanceId);
    const appletOnLoad = (appletObject, ...args) => {
      log('Applet loaded', appletObject);
      resolve(appletObject);
    }
    const applet = new window.GGBApplet(Object.assign({}, options, {appletOnLoad}), '5.0');
    applet.inject(instanceId);
  });
};

// const generateInstanceId = () => 'ggb' + Math.random().toString().slice(2);

class GeogebraPlayer {
  constructor(materialId, instanceId, options = {}) {
    this.materialId = materialId;
    this.instanceId = instanceId;

    this.options = Object.assign({}, {
      materialId: this.materialId,
      instanceId: this.instanceId,
      disableAutoScale: false,
      showToolBar: true,
      scaleContainerClass: 'geogebra-player',
    }, options);

    this.injectApplet();
  }

  injectApplet() {
    if (this.appletInjected) return;
    this.appletInjected = true;
    return createApplet(this.options)
      .then((applet) => (this.applet = applet))
      .then(() => log('applet created', this.materialId))
      .catch(() => (this.appletInjected = false));
  }

}
